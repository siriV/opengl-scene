#version 410 core

out vec4 fColor;

in vec2 fTexCoords;
uniform sampler2D diffuseTexture;

void main() 
{    
    fColor = texture(diffuseTexture, fTexCoords);
}
