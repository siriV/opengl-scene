#version 410 core

in vec3 fPosition;
in vec3 fNormal;
in vec2 fTexCoords;

out vec4 fColor;

//matrices
uniform mat4 model;
uniform mat4 view;
uniform mat3 normalMatrix;
//lighting
uniform vec3 lightDir;
uniform vec3 lightColor;
uniform vec3 campLightColor;
uniform vec3 campLightPos;
uniform vec3 smokeColor;

// textures
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;

//components
vec3 ambient;
float ambientStrength = 0.2f;
vec3 diffuse;
vec3 specular;
float specularStrength = 0.5f;

float constant = 1.0f;
float linear = 0.022f;
float quadratic = 0.0019f;

in vec4 fragPosLightSpace;
uniform sampler2D shadowMap;

float computeShadow()
{
	vec3 normalizedCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
	normalizedCoords = normalizedCoords * 0.5 + 0.5;
	if (normalizedCoords.z > 1.0f)
		return 0.0f;
	float closestDepth = texture(shadowMap, normalizedCoords.xy).r;
	float currentDepth = normalizedCoords.z;
	float shadow = currentDepth - 0.005 > closestDepth ? 1.0 : 0.0;

	return shadow;
}

void computeDirLight()
{
    //compute eye space coordinates
    vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    vec3 normalEye = normalize(normalMatrix * fNormal);

    //normalize light direction
    vec3 lightDirN = vec3(normalize(view * vec4(lightDir, 0.0f)));

    //compute view direction (in eye coordinates, the viewer is situated at the origin
    vec3 viewDir = normalize( - fPosEye.xyz);

    //compute ambient light
    ambient = ambientStrength * lightColor;

    //compute diffuse light
    diffuse = max(dot(normalEye, lightDirN), 0.0f) * lightColor;

    //compute specular light
    vec3 reflectDir = reflect(-lightDirN, normalEye);
    float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), 32);
    specular = specularStrength * specCoeff * lightColor;
}

void computePointLight()
{
	vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
	vec3 normalEye = normalize(normalMatrix * fNormal);

	vec3 lightDirN = normalize(campLightPos -fPosition);
	
	vec3 reflectDir = reflect(-lightDirN, normalEye);
	float specCoeff = pow(max(dot(lightDirN, reflectDir), 0.0f), 32);
	
	float distance = length(campLightPos - fPosition);
	float att = 1.0f/(constant + linear * distance + quadratic * (distance * distance));
	
    ambient += att * ambientStrength * campLightColor;
	diffuse += att * max(dot(normalEye, lightDirN), 0.0f) * campLightColor;
	specular += att * specularStrength* specCoeff * campLightColor;
	
}

float computeFog()
{
 vec4 fPosEye = view * vec4(fPosition, 1.0f);
 float fogDensity = 0.03f;
 float fragmentDistance = length(fPosEye);
 float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2));
 return clamp(fogFactor, 0.0f, 1.0f);
}

void main() 
{
	vec4 colorFromTexture = texture(diffuseTexture, fTexCoords);
	if(colorFromTexture.a < 0.1)
		discard;

    computeDirLight();
	computePointLight();
	ambient *= texture(diffuseTexture, fTexCoords).rgb;
	diffuse *= texture(diffuseTexture, fTexCoords).rgb;
	specular *= texture(specularTexture, fTexCoords).rgb;
	
	float shadow = computeShadow();
	vec3 shadowColor = min((ambient + (1.0f - shadow)*diffuse) + (1.0f - shadow)*specular, 1.0f);

	float fogFactor = computeFog();
	vec4 fogColor = vec4(0.2f, 0.2f, 0.2f, 1.0f);
	fColor = mix(fogColor, vec4(shadowColor,1.0f), fogFactor);
	
	//vec3 color = shadowColor;
	
	//fColor = vec4(color, 1.0f);
    
}
