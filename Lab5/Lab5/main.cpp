#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "glm/glm.hpp" //core glm functionality
#include "glm/gtc/matrix_transform.hpp" //glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp" //glm extension for computing inverse matrices
#include "glm/gtc/type_ptr.hpp" //glm extension for accessing the internal data structure of glm types

#include "Window.h"
#include "Shader.hpp"
#include "Camera.hpp"
#include "Model3D.hpp"

#include <iostream>

// window
gps::Window myWindow;
int modeView = 0;

// matrices
glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;
glm::mat3 normalMatrix;

gps::Model3D scene;
gps::Model3D skyDoom;
gps::Model3D background;
gps::Model3D cauldron;

const unsigned int SHADOW_WIDTH = 2048;
const unsigned int SHADOW_HEIGHT = 2048;
bool showDepthMap = false;

int screenWidth = 1920;
int screenHeigth = 1080;


// light parameters
glm::vec3 lightDir;
glm::vec3 lightColor;
glm::vec3 campLightColor;
glm::vec3 campLightPos = glm::vec3(0.7f, 0.2f, 5.0f);
glm::vec3 cauldronPos = glm::vec3(-3.18f, 4.47f, 40.9f);

// shader uniform locations
GLuint modelLoc;
GLuint viewLoc;
GLuint projectionLoc;
GLuint normalMatrixLoc;
GLuint lightDirLoc;
GLuint lightColorLoc;
GLuint campLightColorLoc;
GLuint campLightPosLoc;
GLuint lightMatrixTrLoc;

gps::Model3D campfireLight;
gps::Shader campfireShader;
gps::Shader skyDoomShader;

gps::Model3D screenQuad;
gps::Shader screenQuadShader;

gps::Model3D hat;


// camera
gps::Camera myCamera(
    glm::vec3(0.1f, 0.0f, 100.4f),
    glm::vec3(0.0f, 0.0f, -10.0f),
    glm::vec3(0.0f, 1.0f, 0.0f));


GLfloat cameraSpeed = 0.1f;

GLboolean pressedKeys[1024];

// models
GLfloat angle;
GLfloat lightPos;

// shaders
gps::Shader myBasicShader;
gps::Shader depthMapShader;

GLuint shadowMapFBO;
GLuint depthMapTexture;

glm::mat4 lightSpaceTrMatrix;
glm::mat4 lightPosition;
glm::vec3 smokeColor;


GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR) {
		std::string error;
		switch (errorCode) {
            case GL_INVALID_ENUM:
                error = "INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                error = "INVALID_VALUE";
                break;
            case GL_INVALID_OPERATION:
                error = "INVALID_OPERATION";
                break;
            case GL_STACK_OVERFLOW:
                error = "STACK_OVERFLOW";
                break;
            case GL_STACK_UNDERFLOW:
                error = "STACK_UNDERFLOW";
                break;
            case GL_OUT_OF_MEMORY:
                error = "OUT_OF_MEMORY";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                error = "INVALID_FRAMEBUFFER_OPERATION";
                break;
        }
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height) {
	fprintf(stdout, "Window resized! New width: %d , and height: %d\n", width, height);
	//TODO
	screenHeigth = height;
	screenWidth = width;
	glViewport(0, 0, screenWidth, screenHeigth);


}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

	if (key == GLFW_KEY_M && action == GLFW_PRESS)
		showDepthMap = !showDepthMap;

	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
		modeView = 1;
	else if (key == GLFW_KEY_2 && action == GLFW_PRESS)
		modeView = 2;
	else if (key == GLFW_KEY_3 && action == GLFW_PRESS)
		modeView = 3;
	else if(key == GLFW_KEY_4 && action == GLFW_PRESS)
		modeView = 4;

	if (key >= 0 && key < 1024) {
        if (action == GLFW_PRESS) {
            pressedKeys[key] = true;
        } else if (action == GLFW_RELEASE) {
            pressedKeys[key] = false;
        }
    }
}

float xpos0, ypos0;
float xanim0, yanim0;
bool firstFrame = true;
bool firstMouse = true;

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
	if (firstMouse == true)
	{
		xpos0 = xpos;
		ypos0 = ypos;
		firstMouse = false;
	}

	float deltaX = xpos - xpos0;
	float deltaY = ypos - ypos0;
	float sensitivity = 0.05f;

	myCamera.rotate(deltaY * sensitivity, deltaX * sensitivity);

	xpos0 = xpos;
	ypos0 = ypos;

	view = myCamera.getViewMatrix();
	myBasicShader.useShaderProgram();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	// compute normal matrix for teapot
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
}

gps::MOVE_DIRECTION getDirection()
{
	if (pressedKeys[GLFW_KEY_W])
		return gps::MOVE_FORWARD;
	if (pressedKeys[GLFW_KEY_S])
		return gps::MOVE_BACKWARD;
	if (pressedKeys[GLFW_KEY_A])
		return gps::MOVE_LEFT;
	return gps::MOVE_RIGHT;

}

bool collision(glm::vec3 nextPos, glm::vec3 p1, glm::vec3 p2)
{
	int col = 0;
	if ((nextPos.x >= p1.x && nextPos.x <= p2.x) || (nextPos.x >= p2.x && nextPos.x <= p1.x))
	{
		col++;
	}
	if ((nextPos.y >= p1.y && nextPos.y <= p2.y) || (nextPos.y >= p2.y && nextPos.y <= p1.y))
	{
		col++;
	}
	if ((nextPos.z >= p1.z && nextPos.z <= p2.z) || (nextPos.z >= p2.z && nextPos.z <= p1.z))
	{
		col++;
	}

	return (col == 3);
}

void processMovement() {

	glm::vec3 firstCenter = myCamera.getCameraNextPosition(getDirection(), cameraSpeed);
	glm::vec3 p1 = glm::vec3(-33.2f, 1.61f, -55.2f);
	glm::vec3 p2 = glm::vec3(-13.3f, 6.2f, -64.04f);

	bool colCart = collision(firstCenter, p1, p2);

	p1 = glm::vec3(-61.8f, 1.18f, -87.6f);
	p2 = glm::vec3(-36.3f, 20.0f, -127.6f);

	bool colChurch = collision(firstCenter, p1, p2);

	p1 = glm::vec3(6.5f, -0.74f, 53.3f);
	p2 = glm::vec3(21.3f, 6.0f, 28.3f);

	bool colHouse = collision(firstCenter, p1, p2);
	if (!colCart && !colChurch && !colHouse)
	{
		if (pressedKeys[GLFW_KEY_W]) {
			myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
			//update view matrix
			view = myCamera.getViewMatrix();
			myBasicShader.useShaderProgram();
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
			// compute normal matrix for teapot
			normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		}

		if (pressedKeys[GLFW_KEY_S]) {
			myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
			//update view matrix
			view = myCamera.getViewMatrix();
			myBasicShader.useShaderProgram();
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
			// compute normal matrix for teapot
			normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		}

		if (pressedKeys[GLFW_KEY_A]) {
			myCamera.move(gps::MOVE_LEFT, cameraSpeed);
			//update view matrix
			view = myCamera.getViewMatrix();
			myBasicShader.useShaderProgram();
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
			// compute normal matrix for teapot
			normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		}

		if (pressedKeys[GLFW_KEY_D]) {
			myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
			//update view matrix
			view = myCamera.getViewMatrix();
			myBasicShader.useShaderProgram();
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
			// compute normal matrix for teapot
			normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		}
	}

	

    
}

void initOpenGLWindow() {

    myWindow.Create(screenWidth, screenHeigth, "OpenGL Project Core");
}

void setWindowCallbacks() {
	glfwSetWindowSizeCallback(myWindow.getWindow(), windowResizeCallback);
    glfwSetKeyCallback(myWindow.getWindow(), keyboardCallback);
    glfwSetCursorPosCallback(myWindow.getWindow(), mouseCallback);
	glfwSetInputMode(myWindow.getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void initOpenGLState() {
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
    glEnable(GL_FRAMEBUFFER_SRGB);
	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

gps::Model3D nanosuit;

void initModels() {
	scene.LoadModel("objects/scene.obj", "textures/");
	skyDoom.LoadModel("objects/skyDoom.obj", "textures/");
	hat.LoadModel("objects/hat.obj", "textures/");
	cauldron.LoadModel("objects/cauldron.obj", "textures/");
}

void initShaders() {
	myBasicShader.loadShader("shaders/basic.vert","shaders/basic.frag");
	myBasicShader.useShaderProgram();
	campfireShader.loadShader("shaders/campfire.vert", "shaders/campfire.frag");
	campfireShader.useShaderProgram();
	screenQuadShader.loadShader("shaders/screenQuad.vert", "shaders/screenQuad.frag");
	screenQuadShader.useShaderProgram();
	depthMapShader.loadShader("shaders/depthMap.vert", "shaders/depthMap.frag");
	depthMapShader.useShaderProgram();
	skyDoomShader.loadShader("shaders/skyDoom.vert", "shaders/skyDoom.frag");
	skyDoomShader.useShaderProgram();
}

void initUniforms() {
	myBasicShader.useShaderProgram();

    // create model matrix for teapot
    model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
	modelLoc = glGetUniformLocation(myBasicShader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	// get view matrix for current camera
	view = myCamera.getViewMatrix();
	viewLoc = glGetUniformLocation(myBasicShader.shaderProgram, "view");
	// send view matrix to shader
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

    // compute normal matrix for teapot
    normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	normalMatrixLoc = glGetUniformLocation(myBasicShader.shaderProgram, "normalMatrix");
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));

	// create projection matrix
	projection = glm::perspective(glm::radians(45.0f),
                               (float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height,
                               0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "projection");
	// send projection matrix to shader
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));	

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 5.0f, 1.0f);
	lightPosition = glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	lightDirLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightDir");
	// send light dir to shader
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::inverseTranspose(glm::mat3(view * lightPosition)) * lightDir));

	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightColor");
	// send light color to shader
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	campLightPos = glm::vec3(-3.0f, 0.06f, 41.54f);
	campLightPosLoc = glGetUniformLocation(myBasicShader.shaderProgram, "campLightPos");
	glUniform3fv(campLightPosLoc, 1, glm::value_ptr(campLightPos));


	campLightColor = glm::vec3(1.0f, 0.0f, 0.0f);
	campLightColorLoc = glGetUniformLocation(myBasicShader.shaderProgram, "campLightColor");
	glUniform3fv(campLightColorLoc, 1, glm::value_ptr(campLightColor));

	campfireShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(campfireShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	skyDoomShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(skyDoomShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
}

void initFBO() {
	glGenFramebuffers(1, &shadowMapFBO);
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture,
		0);

	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}


glm::mat4 computeLightSpaceTrMatrix() {
	glm::vec3 cameraPosition = myCamera.getCameraPosition();
	glm::vec3 shadowDir = glm::vec3(-3.3f, 3.5f, 50.97f);

	glm::mat4 lightView = glm::lookAt(glm::inverseTranspose(glm::mat3(lightPosition)) * shadowDir, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	const GLfloat near_plane = 0.1f, far_plane = 100.0f;
	glm::mat4 lightProjection = glm::ortho(-4.0f, 3.0f, -5.0f, 8.0f, near_plane, far_plane);
	glm::mat4 lightSpaceTrMatrix = lightProjection * lightView;

	return lightSpaceTrMatrix;
}

bool depth;

float angleC = 0.0f;
bool goRight = true;

void renderTeapot(gps::Shader shader) {
	
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f));
	modelLoc = glGetUniformLocation(shader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	if (!depth)
	{
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	}
	
	scene.Draw(shader);
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-0.2f, 3.3f, -0.7f));
	model = glm::translate(model, campLightPos);
	model = glm::rotate(model, glm::radians(angleC), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	model = glm::translate(model, glm::vec3(0.2f, -3.3f, 0.7f));
	glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	if (!depth)
	{
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	}

	cauldron.Draw(shader);
}


void renderScene() {

	depthMapShader.useShaderProgram();
	lightMatrixTrLoc = glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix");
	glUniformMatrix4fv(lightMatrixTrLoc, 1, GL_FALSE, glm::value_ptr(computeLightSpaceTrMatrix()));
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	depth = true;
	renderTeapot(depthMapShader);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	

	//render the scene

	// render the teapot

	if (showDepthMap) {

		glViewport(0, 0, screenWidth, screenHeigth);

		glClear(GL_COLOR_BUFFER_BIT);

		screenQuadShader.useShaderProgram();

		//bind the depth map
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, depthMapTexture);
		glUniform1i(glGetUniformLocation(screenQuadShader.shaderProgram, "depthMap"), 0);

		glDisable(GL_DEPTH_TEST);
		screenQuad.Draw(screenQuadShader);
		glEnable(GL_DEPTH_TEST);



	}
	else
	{
		glViewport(0, 0, screenWidth, screenHeigth);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		myBasicShader.useShaderProgram();
		depth = false;


		view = myCamera.getViewMatrix();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		lightPosition = glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::inverseTranspose(glm::mat3(view * lightPosition)) * lightDir));

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, depthMapTexture);
		glUniform1i(glGetUniformLocation(myBasicShader.shaderProgram, "shadowMap"), 3);

		glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightSpaceTrMatrix"),
			1,
			GL_FALSE,
			glm::value_ptr(computeLightSpaceTrMatrix()));

		renderTeapot(myBasicShader);

		campfireShader.useShaderProgram();

		angle += 1.0f;
		if (angleC >= 15.0f || angleC <= -15.0f)
		{
			goRight = !goRight;
		}

		if (goRight)
		{
			angleC += 0.25f;
		}
		else
		{
			angleC -= 0.25f;
		}

		glUniformMatrix4fv(glGetUniformLocation(campfireShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
		model = glm::mat4(1.0f);
		model = glm::translate(model, campLightPos);
		model = glm::rotate(model, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::translate(model, glm::vec3(3.0f, 0.0f, -1.0f));
		model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
		model = glm::rotate(model, glm::radians(270.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(glGetUniformLocation(campfireShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

		hat.Draw(campfireShader);

		skyDoomShader.useShaderProgram();

		glUniformMatrix4fv(glGetUniformLocation(skyDoomShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
		model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(glGetUniformLocation(skyDoomShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

		skyDoom.Draw(skyDoomShader);
		
		
	}
}

void cleanup() {
    myWindow.Delete();
    //cleanup code for your own data
}

int main(int argc, const char * argv[]) {

    try {
        initOpenGLWindow();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    initOpenGLState();
	initModels();
	initShaders();
	initUniforms();
	initFBO();
    setWindowCallbacks();

	glCheckError();
	// application loop
	while (!glfwWindowShouldClose(myWindow.getWindow())) {
		switch (modeView)
		{
		case 1: 
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			break;
		case 2:
			glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
			break;
		case 3:
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glShadeModel(GL_SMOOTH);
			break;
		default:
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glEnable(GL_POLYGON_OFFSET_FILL);
			glPolygonOffset(1, 1);
		}
        processMovement();
	    renderScene();

		glfwPollEvents();
		glfwSwapBuffers(myWindow.getWindow());

		glCheckError();
	}

	cleanup();

    return EXIT_SUCCESS;
}
