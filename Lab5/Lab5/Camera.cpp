#include "Camera.hpp"

namespace gps {

    //Camera constructor
    Camera::Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget, glm::vec3 cameraUp) {
        this->cameraPosition = cameraPosition;
        this->cameraTarget = cameraTarget;
        this->cameraUpDirection = cameraUp;
		this->cameraAnglePitch = 0.0f;
		this->cameraAngleYaw = -90.0f;
        //TODO - Update the rest of camera parameters

    }

    //return the view matrix, using the glm::lookAt() function
    glm::mat4 Camera::getViewMatrix() {
        return glm::lookAt(cameraPosition, cameraTarget, cameraUpDirection);
    }

	glm::vec3 Camera::getCameraPosition()
	{
		return this->cameraPosition;
	}

	glm::vec3 Camera::getCameraTarget()
	{
		return this->cameraTarget;
	}

	glm::vec3 Camera::getCameraNextPosition(MOVE_DIRECTION direction, float speed)
	{
		switch (direction)
		{
		case MOVE_FORWARD:
			return (this->cameraPosition + this->cameraFrontDirection * speed);
		case MOVE_BACKWARD:
			return (this->cameraPosition - this->cameraFrontDirection * speed);
		case MOVE_LEFT:
			return (this->cameraPosition - glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection)) * speed);
		case MOVE_RIGHT:
			return (this->cameraPosition + glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection)) * speed);
		}
	}

    //update the camera internal parameters following a camera move event
    void Camera::move(MOVE_DIRECTION direction, float speed) {
		switch (direction)
		{
		case MOVE_FORWARD:
			this->cameraPosition += this->cameraFrontDirection * speed;
			this->cameraTarget += this->cameraFrontDirection * speed;
			break;
		case MOVE_BACKWARD:
			this->cameraPosition -= this->cameraFrontDirection * speed;
			this->cameraTarget -= this->cameraFrontDirection * speed;
			break;
		case MOVE_LEFT:
			this->cameraPosition -= glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection)) * speed;
			this->cameraTarget -= glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection)) * speed;
			break;
		case MOVE_RIGHT:
			this->cameraPosition += glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection)) * speed;
			this->cameraTarget += glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection)) * speed;
			break;
		}

		this->cameraFrontDirection = glm::normalize(this->cameraTarget - this->cameraPosition);
    }

    //update the camera internal parameters following a camera rotate event
    //yaw - camera rotation around the y axis
    //pitch - camera rotation around the x axis
    void Camera::rotate(float pitch, float yaw) {
		cameraAngleYaw += yaw;
		cameraAnglePitch += pitch * greater;
		this->cameraTarget.x = this->cameraPosition.x + cos(glm::radians(cameraAngleYaw)) * cos(glm::radians(cameraAnglePitch));
		this->cameraTarget.z = this->cameraPosition.z + sin(glm::radians(cameraAngleYaw)) * cos(glm::radians(cameraAnglePitch));;
		this->cameraTarget.y = this->cameraPosition.y + sin(glm::radians(cameraAnglePitch));

		this->cameraFrontDirection = glm::normalize(this->cameraTarget - this->cameraPosition);
		this->cameraRightDirection = glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection));
    }
}