#version 410 core

out vec4 fColor;
in vec2 fTexCoords;
uniform sampler2D diffuseTexture;

in vec3 fPosition;

uniform mat4 view;

float computeFog()
{
 vec4 fPosEye = view * vec4(fPosition, 1.0f);
 float fogDensity = 0.03f;
 float fragmentDistance = length(fPosEye);
 float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2));

 return clamp(fogFactor, 0.0f, 1.0f);
}

void main() 
{    
    	vec4 color = texture(diffuseTexture, fTexCoords);
	float fogFactor = computeFog();
	vec4 fogColor = vec4(0.2f, 0.2f, 0.2f, 1.0f);
	fColor = mix(fogColor, color, fogFactor);
}
